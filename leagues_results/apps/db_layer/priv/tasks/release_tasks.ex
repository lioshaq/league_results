defmodule DbLayer.ReleaseTasks do
  @moduledoc """
  Release tasks
  """
  @start_apps [
    :crypto,
    :ssl,
    :postgrex,
    :ecto
  ]

  def db_layer, do: :db_layer

  def repos, do: Application.get_env(db_layer(), :ecto_repos, [])

  def setup do
    load_app()

    # Run migrations
    migrate()

    # Run seed script
    Enum.each(repos(), &run_seeds_for/1)

    # Signal shutdown
    IO.puts("Success!")
    :init.stop()
  end

  def seed do
    load_app()

    # Run migrations
    migrate()

    # Run seed script
    Enum.each(repos(), &run_seeds_for/1)

    # Signal shutdown
    IO.puts("Success!")
    :init.stop()
  end

  def migrate, do: Enum.each(repos(), &run_migrations_for/1)

  def priv_dir(app), do: "#{:code.priv_dir(app)}"

  defp run_migrations_for(repo) do
    app = Keyword.get(repo.config, :otp_app)
    IO.puts("Running migrations for #{app}")
    Ecto.Migrator.run(repo, migrations_path(repo), :up, all: true)
  end

  def run_seeds_for(repo) do
    # Run the seed script if it exists
    seed_script = seeds_path(repo)

    if File.exists?(seed_script) do
      IO.puts("Running seed script..")
      Code.eval_file(seed_script)
    end
  end

  def migrations_path(repo), do: priv_path_for(repo, "migrations")

  def seeds_path(repo), do: priv_path_for(repo, "seeds.exs")

  def priv_path_for(repo, filename) do
    app = Keyword.get(repo.config, :otp_app)
    repo_underscore = repo |> Module.split() |> List.last() |> Macro.underscore()
    Path.join([priv_dir(app), repo_underscore, filename])
  end

  defp create_storage_for(repo) do
    info("Creating database for #{inspect(repo)}..")
    IO.inspect repo.config
    case repo.__adapter__.storage_up(repo.config) do
      :ok ->
        info("The database for #{inspect(repo)} has been created.")

      {:error, :already_up} ->
        info("The database for #{inspect(repo)} has already been created.")

      {:error, term} when is_binary(term) ->
        fatal("The database for #{inspect(repo)} couldn't be created, reason given: #{term}.")

      {:error, term} ->
        fatal(
          "The database for #{inspect(repo)} couldn't be created, reason given: #{inspect(term)}."
        )

      error ->
        IO.inspect error
    end
  end

  defp load_app do
    me = db_layer()

    IO.puts("Loading #{me}..")
    # Load the code for db_layer , but don't start it
    :ok = Application.load(me)

    IO.puts("Starting dependencies..")
    # Start apps necessary for executing migrations
    Enum.each(@start_apps, &Application.ensure_all_started/1)


    IO.puts("Create repo #{inspect repos()}..")
    # Ensure database is created
    Enum.each(repos(), &create_storage_for/1)

    # Start the Repo(s) for db_layer
    IO.puts("Starting repos..")
    Enum.each(repos(), & &1.start_link(pool_size: 1))
  end

  defp info(message) do
    IO.puts(message)
  end

  defp fatal(message) do
    IO.puts(:stderr, message)
    System.halt(1)
  end
end
