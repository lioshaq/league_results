defmodule DbLayer.Repo.Migrations.Matches do
  use Ecto.Migration

  def change do

    create table(:matches) do
      add(:division, :string)
      add(:season, :string)
      add(:match_date, :date)
      add(:home_goals_final, :integer)
      add(:away_goals_final, :integer)
      add(:home_goals_half, :integer)
      add(:away_goals_half, :integer)
      add(:home_team_id, references(:teams, on_delete: :nothing))
      add(:away_team_id, references(:teams, on_delete: :nothing))
      timestamps()
    end

    create(unique_index(:matches, [:match_date, :home_team_id, :away_team_id]))
  end
end
