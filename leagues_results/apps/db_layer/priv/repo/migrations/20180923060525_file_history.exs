defmodule DbLayer.Repo.Migrations.FileHistory do
  use Ecto.Migration

  def change do

    create table(:file_history) do
      add(:file_name, :string)
      timestamps()
    end
    create(unique_index(:file_history, [:file_name]))
  end
end


