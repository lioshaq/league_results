alias DbLayer.Domain.Team
alias DbLayer.Repo


teams = [
  %{ name: "Alaves" },
  %{ name: "Albacete" },
  %{ name: "Alcorcon" },
  %{ name: "Almeria" },
  %{ name: "Arsenal" },
  %{ name: "Ath Bilbao" },
  %{ name: "Ath Bilbao B" },
  %{ name: "Ath Madrid" },
  %{ name: "Augsburg" },
  %{ name: "Barcelona" },
  %{ name: "Bayern Munich" },
  %{ name: "Betis" },
  %{ name: "Bournemouth" },
  %{ name: "Burnley" },
  %{ name: "Cadiz" },
  %{ name: "Celta" },
  %{ name: "Chelsea" },
  %{ name: "Cordoba" },
  %{ name: "Crystal Palace" },
  %{ name: "Darmstadt" },
  %{ name: "Dortmund" },
  %{ name: "Eibar" },
  %{ name: "Ein Frankfurt" },
  %{ name: "Elche" },
  %{ name: "Espanol" },
  %{ name: "Everton" },
  %{ name: "FC Koln" },
  %{ name: "Freiburg" },
  %{ name: "Getafe" },
  %{ name: "Gimnastic" },
  %{ name: "Girona" },
  %{ name: "Granada" },
  %{ name: "Hamburg" },
  %{ name: "Hertha" },
  %{ name: "Hoffenheim" },
  %{ name: "Huesca" },
  %{ name: "Hull" },
  %{ name: "Ingolstadt" },
  %{ name: "La Coruna" },
  %{ name: "Las Palmas" },
  %{ name: "Leganes" },
  %{ name: "Leicester" },
  %{ name: "Levante" },
  %{ name: "Leverkusen" },
  %{ name: "Liverpool" },
  %{ name: "Llagostera" },
  %{ name: "Lugo" },
  %{ name: "Mainz" },
  %{ name: "Malaga" },
  %{ name: "Mallorca" },
  %{ name: "Man City" },
  %{ name: "Man United" },
  %{ name: "M'gladbach" },
  %{ name: "Middlesbrough" },
  %{ name: "Mirandes" },
  %{ name: "Numancia" },
  %{ name: "Osasuna" },
  %{ name: "Oviedo" },
  %{ name: "Oviedo " },
  %{ name: "Ponferradina" },
  %{ name: "RB Leipzig" },
  %{ name: "Real Madrid" },
  %{ name: "Reus Deportiu" },
  %{ name: "Schalke 04" },
  %{ name: "Sevilla" },
  %{ name: "Sevilla B" },
  %{ name: "Sociedad" },
  %{ name: "Southampton" },
  %{ name: "Sp Gijon" },
  %{ name: "Stoke" },
  %{ name: "Sunderland" },
  %{ name: "Swansea" },
  %{ name: "Tenerife" },
  %{ name: "Tottenham" },
  %{ name: "UCAM Murcia" },
  %{ name: "Valencia" },
  %{ name: "Valladolid" },
  %{ name: "Vallecano" },
  %{ name: "Villarreal" },
  %{ name: "Watford" },
  %{ name: "Werder Bremen" },
  %{ name: "West Brom" },
  %{ name: "West Ham" },
  %{ name: "Wolfsburg" },
  %{ name: "Zaragoza" }
]

IO.puts("Seeding teams ...")

teams
|> Task.async_stream(
  fn c ->
    unless Repo.get_by(Team, name: c.name) do
      # team not found, we build one

      %Team{}
      |> Team.changeset(c)
      |> Repo.insert!()
    end
  end,
  max_concurrency: 10,
  timeout: :infinity
)
|> Enum.to_list()

IO.puts("Seeding matches")

priv_dir = :db_layer |> :code.priv_dir()
seed_dir = Path.join([priv_dir, "repo", "data.csv"])
seed_dir
|> DbLayer.Parser.Reader.process_file()
|> DbLayer.Parser.Reader.import_file()
