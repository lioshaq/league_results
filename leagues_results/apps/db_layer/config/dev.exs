use Mix.Config


config :db_layer, DbLayer.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "db_layer_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool_size: 15,
  ownership_timeout: 60_000,
  timeout: 60_000,
  pool_timeout: 60_000
