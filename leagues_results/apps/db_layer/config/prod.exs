use Mix.Config

config :db_layer, DbLayer.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: "${DB_HOST}",
  database: "${DB_NAME}",
  username: "${DB_USER}",
  password: "${DB_PASSWORD}",
  pool_size: 15,
  ownership_timeout: 60_000,
  timeout: 60_000,
  pool_timeout: 60_000
