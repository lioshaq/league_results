defmodule DbLayer do
  @moduledoc """
  Documentation for DbLayer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DbLayer.hello
      :world

  """
  def hello do
    :world
  end
end
