defmodule DbLayer.Match.Queries do
  @moduledoc """
    List of queries for matches
  """
  alias DbLayer.Repo
  alias DbLayer.Domain.{Match, Team}
  import Ecto.Query

  require Logger

  @doc """
    Gets the parameters send in request and returns the results if params emptry
    the function returns all matches
  """
  @spec list_matches_by(map()) :: list
  def list_matches_by(%{"team_a" => team_name_a, "team_b" => team_name_b} = params) do
    team_a = find_team(team_name_a)
    team_b = find_team(team_name_b)
    basic_filters = get_filters_from_params(params)

    filter_a = basic_filters ++ [away_team_id: team_a.id, home_team_id: team_b.id]
    filter_b = basic_filters ++ [away_team_id: team_b.id, home_team_id: team_a.id]
    Logger.debug("Found filters #{inspect(basic_filters)}")

    query = from(m in Match, where: ^filter_a, or_where: ^filter_b)

    query
    |> Repo.all()
    |> Repo.preload([:away_team, :home_team])
  end

  @spec list_matches_by(map()) :: list
  def list_matches_by(params) do

    filters = get_filters_from_params(params)

    query = from(m in Match, where: ^filters)

    query
    |> Repo.all()
    |> Repo.preload([:away_team, :home_team])
  end

  @spec get_match!(string()) :: %Match{}
  def get_match!(id) do
    Match
    |> Repo.get!(id)
    |> Repo.preload([:away_team, :home_team])
  end

  @available_filters ["season", "division"]
  defp get_filters_from_params(%{}) do
    []
  end


  # searches in params available filters
  # is used to avoid error when passing wrong params

  @spec list_matches_by(map()) :: list
  defp get_filters_from_params(params) do
    @available_filters
    |> Enum.reduce(Keyword.new(), fn filter, acc ->
      case Map.get(params, filter) do
        nil -> acc
        value ->
          Keyword.put(acc, String.to_atom(filter), value)
      end
    end)
  end

  defp find_team(name) do
    Team
    |> Repo.get_by!(name: String.trim(name))
  end

end
