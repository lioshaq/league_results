defmodule DbLayer.Domain.Match do
  use Ecto.Schema
  import Ecto.Changeset

  schema "matches" do
    field :division, :string
    field :season, :string
    field :match_date, :date
    field :home_goals_final, :integer
    field :away_goals_final, :integer
    field :home_goals_half, :integer
    field :away_goals_half, :integer
    belongs_to :home_team, DbLayer.Domain.Team
    belongs_to :away_team, DbLayer.Domain.Team

    timestamps()
  end

  def changeset(match, params \\ %{}) do
    match
    |> cast(params, [:division, :season, :match_date, :home_goals_final, :away_goals_final, :home_goals_half, :away_goals_half])
    |> validate_required([:division, :season, :match_date, :home_goals_final, :away_goals_final, :home_goals_half, :away_goals_half])
  end

end
