defmodule DbLayer.Domain.FileHistory do
  use Ecto.Schema
  import Ecto.Changeset

  schema "file_history" do
    field :file_name, :string

    timestamps()
  end

  def changeset(team, params \\ %{}) do
    team
    |> cast(params, [:file_name])
    |> validate_required([:file_name])
  end
end
