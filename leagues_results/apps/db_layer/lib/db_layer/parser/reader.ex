defmodule DbLayer.Parser.Reader do
  @moduledoc """
  """
  alias DbLayer.Repo
  alias DbLayer.Domain.FileHistory
  require Logger

  def process_file(path) do
    case Repo.get_by(FileHistory, file_name: path) do
      nil -> {:ok, path}
      _ -> {:error, path}
    end
  end

  def import_file({:error, path}) do
    Logger.info(fn -> "File #{path} already imported" end)
  end

  def import_file({:ok, path}) do
    # get number of lines to skip(header lines)
    skip = DbLayer.Parser.CsvParser.skip_lines()
    # get list og headers for this csv file. Order of elements is important
    headers = DbLayer.Parser.CsvParser.headers()
    headers_count = Enum.count(headers)

    # stream line by line from csv file
    # |> File.stream!([read_ahead: 100_000, trim_bom: true], :line)
    # Skip CSV header lines
    # parse cvs lines

    path
    |> File.stream!()
    |> Stream.drop(skip)
    |> CsvParser.parse_stream(headers: false)
    |> Stream.map(fn fields ->
      actual_count = Enum.count(fields)

      case actual_count == headers_count do
        true ->
          {:ok, headers |> Enum.zip(fields) |> Enum.into(%{}), fields}

        false ->
          {:error, "Row has #{actual_count} columns - expected #{headers_count}", fields}
      end
    end)
    |> Task.async_stream(&process_csv_line(&1), max_concurrency: 10, timeout: 80_000)
    |> Stream.run()

    %FileHistory{}
    |> FileHistory.changeset(%{file_name: path})
    |> Repo.insert!
  end

  defp process_csv_line({:ok, decoded, _fields}) do
    try do
      DbLayer.Parser.CsvParser.create(decoded)
    rescue
      ex ->
        Logger.error("Error in line #{inspect decoded}")
        Logger.error("Error description #{inspect ex}")
    end
  end

end
