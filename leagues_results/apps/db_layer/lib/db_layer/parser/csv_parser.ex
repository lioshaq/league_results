defmodule DbLayer.Parser.CsvParser do
  @moduledoc """
  Import content of CSV file into matches table
  Following table contains mapping information between CSV and table

  | CSV                  | CSV sample line                | Match                |
  | -------------------- | ------------------------------ | -------------------- |
  | id                   | 1                              | ignore               |
  | div                  | SP1                            | Division             |
  | season               | 201617                         | season               |
  | date                 | 28/08/16                       | match_date           |
  | HomeTeam             | La Coruna                      | home_team            |
  | AwayTeam             | Eibar                          | away_team            |
  | FTHG                 | 1                              | home_goals_final     |
  | FTAG                 | 1                              | away_goals_final     |
  | FTR                  | D                              | ignore               |
  | HTHG                 | 2                              | home_goals_half      |
  | HTAG                 | 2                              | away_goals_half      |
  | HTR                  | 1                              | ignore              |

  """
  alias DbLayer.Domain.{Match, Team}
  alias Ecto.Changeset
  alias DbLayer.Repo
  require Logger

  NimbleCSV.define(CsvParser, separator: ",", escape: "\"")

  @csv_date_long_format "{D}/{M}/{YYYY}"
  @csv_date_format "{D}/{M}/{YY}"
  @doc "
  Map CSV headers to db fields
  "
  def headers,
    do:
      [
        id: :ignore,
        div: :division,
        season: :season,
        date: :match_date,
        HomeTeam: :home_team,
        AwayTeam: :away_team,
        FTHG: :home_goals_final,
        FTAG: :away_goals_final,
        FTR: :ignore,
        HTHG: :home_goals_half,
        HTAG: :away_goals_half,
        HTR: :ignore,
      ]
      |> Keyword.values()

  def separator(), do: ?,
  def skip_lines(), do: 1

  def create(params) do
    # convert string into a datetime
    {_, params} =
      Map.get_and_update(params, :match_date, fn d ->
        # check if year is 2 digit
        case Regex.match?(~r/\/(\d)(\d)$/, d) do
          false -> {d, Timex.parse!(d, @csv_date_long_format)}
          _ -> {d, Timex.parse!(d, @csv_date_format)}
        end
      end)

      home_team = Repo.get_by(Team, name: String.trim(params.home_team))
      away_team = Repo.get_by(Team, name: String.trim(params.away_team))

      Logger.debug(fn -> "Adding match to db #{inspect(params)}" end)

      %Match{}
        |> Match.changeset(params)
        |> Changeset.put_assoc(:home_team, home_team)
        |> Changeset.put_assoc(:away_team, away_team)
        |> Repo.insert!(timeout: :infinity, pool_timeout: :infinity)


  end

end
