# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :result_json_api,
  namespace: ResultJsonApi

# Configures the endpoint
config :result_json_api, ResultJsonApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "A197Ryv01XZFYbwUC0ziu2c4DaeEplnGCDHRZ+AKZffrZdfCazYNvL4FfA6nlZos",
  render_errors: [view: ResultJsonApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ResultJsonApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
