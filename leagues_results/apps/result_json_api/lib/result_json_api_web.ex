defmodule ResultJsonApiWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use ResultJsonApiWeb, :controller
      use ResultJsonApiWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: ResultJsonApiWeb
      import Plug.Conn
      import ResultJsonApiWeb.Router.Helpers
      import ResultJsonApiWeb.Gettext
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "lib/result_json_api_web/templates",
                        namespace: ResultJsonApiWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      import ResultJsonApiWeb.Router.Helpers
      import ResultJsonApiWeb.ErrorHelpers
      import ResultJsonApiWeb.Gettext
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import ResultJsonApiWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
