defmodule Protobuf.ProDef do
  use Protobuf, """
    message Match {
      required int32 id = 1;
      required string match_date = 2;
      required string season = 3;
      required string home_team = 4;
      required int32 home_goals_half = 5;
      required int32 home_goals_final = 6;
      required string division = 7;
      required string away_team = 8;
      required int32 away_goals_half = 9;
      required int32 away_goals_final = 10;
    }
  """

  def safe_decode(bytes) do
    try do
      {:ok, Protobuf.ProDef.Match.decode_delimited(bytes)}
    rescue
      ErlangError ->
        {:error, "Error encoging data"}
    end
  end

end
