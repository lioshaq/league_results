defmodule ResultJsonApiWeb.MatchController do
  use ResultJsonApiWeb, :controller

  alias DbLayer.Match.Queries, as: MatchQueries

  action_fallback ResultJsonApiWeb.FallbackController

  def index(conn, params) do
    try do
      matches = MatchQueries.list_matches_by(params)
      render(conn, "index.json", matches: matches)
    rescue
      _ex in Ecto.NoResultsError ->
        render(conn, "index.json", matches: [])
    end
  end


  def show(conn, %{"id" => id}) do
    try do
      match = MatchQueries.get_match!(id)
      render(conn, "show.json", match: match)
    rescue
      _ex in Ecto.NoResultsError ->
        {:error, :not_found}
    end
  end
end
