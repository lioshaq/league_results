defmodule ResultJsonApiWeb.ProtoMatchController do
  use ResultJsonApiWeb, :controller

  alias DbLayer.Match.Queries, as: MatchQueries

  action_fallback ResultJsonApiWeb.FallbackController

  def index(conn, params) do
    response =
      params
      |> MatchQueries.list_matches_by
      |> Enum.reduce([], fn match, acc ->
        [create_proto_buf(match) | acc]
      end)
      |> Protobuf.ProDef.Match.encode_delimited()

    conn
    |> put_resp_header("content-type", "application/octet-stream")
    |> send_resp(200, response)
  end

  def show(conn, %{"id" => id}) do
    try do
      response =
        id
        |> MatchQueries.get_match!()
        |> create_proto_buf()
        |> Protobuf.ProDef.Match.encode()

      conn
        |> put_resp_header("content-type", "application/octet-stream")
        |> send_resp(200, response)
    rescue
      _ex in Ecto.NoResultsError ->
        {:error, :not_found}
    end
  end

  defp create_proto_buf(match) do
    Protobuf.ProDef.Match.new(
      id: match.id,
      match_date: Date.to_string(match.match_date),
      season: match.season,
      home_team: match.home_team.name,
      home_goals_half: match.home_goals_half,
      home_goals_final: match.home_goals_final,
      division: match.division,
      away_team: match.away_team.name,
      away_goals_half: match.away_goals_half,
      away_goals_final: match.away_goals_final
    )

  end
end
