defmodule ResultJsonApiWeb.Router do
  use ResultJsonApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ResultJsonApiWeb do
    pipe_through :api

    get "/matches", MatchController, :index
    get "/matches/:id", MatchController, :show
  end

  scope "/api_v2", ResultJsonApiWeb do
    pipe_through :api

    get "/matches", ProtoMatchController, :index
    get "/matches/:id", ProtoMatchController, :show
  end

end
