defmodule ResultJsonApiWeb.MatchView do
  use ResultJsonApiWeb, :view
  alias ResultJsonApiWeb.MatchView

  def render("index.json", %{matches: matches}) do
    %{data: render_many(matches, MatchView, "match.json")}
  end

  def render("show.json", %{match: match}) do
    %{data: render_one(match, MatchView, "match.json")}
  end

  def render("match.json", %{match: match}) do
    %{id: match.id,
      division: match.division,
      season: match.season,
      match_date: match.match_date,
      home_goals_final: match.home_goals_final,
      away_goals_final: match.away_goals_final,
      home_team: match.home_team.name,
      away_team: match.away_team.name,
      home_goals_half: match.home_goals_half,
      away_goals_half: match.away_goals_half,
    }
  end
end
