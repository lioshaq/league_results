defmodule ResultJsonApiWeb.MatchControllerTest do
  use ResultJsonApiWeb.ConnCase

  alias DbLayer.Repo
  alias Ecto.Adapters.SQL.Sandbox
  alias DbLayer.Domain.{Match, Team}
  @create_attrs %{away_goals_final: 3, division: "SP1", home_goals_final: 2, match_date: ~D[2010-04-17], season: "201617",
                    away_goals_half: 2, home_goals_half: 0}

  def fixture(:match) do
    team_a = Repo.insert!(Team.changeset(%Team{}, %{name: "Test1"}))
    team_b = Repo.insert!(Team.changeset(%Team{}, %{name: "Test2"}))

    match =
      %Match{}
      |> Match.changeset(@create_attrs)
      |> Ecto.Changeset.put_assoc(:home_team, team_a)
      |> Ecto.Changeset.put_assoc(:away_team, team_b)
      |> Repo.insert!()

  end

  setup %{conn: conn} do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(Repo)
    Sandbox.mode(Repo, {:shared, self()})

    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    setup [:create_match]

    test "lists all matches", %{conn: conn, match: match} do

      conn_all = get conn, match_path(conn, :index)

      assert json_response(conn_all, 200)["data"] |> length == 1

      conn_teams = get conn, match_path(conn, :index, %{team_a: "Test2", team_b: "Test3"})

      assert json_response(conn_teams, 200)["data"] |> length == 0

      team_conn = get conn, match_path(conn, :index, %{team_a: "Test2", team_b: "Test1"})

      assert json_response(team_conn, 200)["data"] |> length == 1

      team_conn = get conn, match_path(conn, :index, %{season: "201617", division: "SP1"})

      assert json_response(team_conn, 200)["data"] |> length == 1


    end
  end


  defp create_match(_) do
    match = fixture(:match)
    {:ok, match: match}
  end
end
