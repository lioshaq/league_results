echo $DB_HOST
echo $DB_USER
echo $DB_PASSWORD
until PGPASSWORD=$DB_PASSWORD psql -h $DB_HOST -U $DB_USER -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"

echo "Executing pre_start plugin!"
$RELEASE_ROOT_DIR/bin/leagues_results command Elixir.DbLayer.ReleaseTasks setup
