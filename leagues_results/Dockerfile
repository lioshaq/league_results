FROM elixir:1.6.6-alpine as builder
ENV HOME=/opt/app/ TERM=xterm

# Install Elixir and basic build dependencies
RUN \
    echo "@edge http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories && \
    apk --update upgrade musl && \
    apk --no-cache add \
      git make g++ && \
    rm -rf /var/cache/apk/*

# Install Hex+Rebar
RUN mix local.hex --force && \
    mix local.rebar --force

WORKDIR /opt/app

ENV MIX_ENV=prod

# Cache elixir deps
COPY mix.exs mix.lock ./
COPY config config

COPY apps apps

RUN mix do deps.get, deps.compile

COPY rel rel


RUN mix release --env=prod --verbose

FROM alpine:3.7

RUN apk --no-cache --update add libgcc libstdc++ openssl ncurses-libs bash postgresql-client

WORKDIR /opt/leagues_results/

EXPOSE 4001
ENV PORT=4001 MIX_ENV=prod REPLACE_OS_VARS=true

COPY --from=builder /opt/app/_build/prod/rel/leagues_results/releases/0.1.0/leagues_results.tar.gz .
RUN tar zxf leagues_results.tar.gz && rm leagues_results.tar.gz

USER root
ENTRYPOINT ["/opt/leagues_results/bin/leagues_results"]

